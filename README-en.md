# Arithmetic Expressions - Pretty Plots

![rsz_heart.png](images/rsz_heart.png)
![rsz_mariposa1.png](images/rsz_mariposa1.png)
![rsz_mariposa.png](images/rsz_mariposa.png)



Arithmetic expressions are an essential part of almost any algorithm that solves a useful problem. Therefore, a basic skill in any computer programming language is to implement arithmetic expressions correctly. In this laboratory experience you will practice the implementation of arithmetic expressions in C++ by writing parametric equations to plot interesting curves.

## Objectives:

1. Implement arithmetic expressions in C++ to produce graphs.
2. Use constants adequately.
3. Define variables using adequate data types.
4. Cast a data value to another type when necessary.


## Pre-Lab:

Before you get to the laboratory you should have:

1. Reviewed the following concepts:

	a. Implementing arithmetic expressions in C++

	b. Basic data types in C++ (int, float, double)

	c. Using  "type casting" to cast the value of variables to other data types within expressions

	d. Using arithmetic functions and constants from the `cmath` library

	e. The equation and graph of a circle.

2. Studied the concepts and instructions for the laboratory session

3. Taken the Pre-Lab quiz, available in Moodle.

---

---


## Parametric Equations

*Parametric equations* allow us to represent a quantity as a function of one or more independent variables called *parameters*. In many occasions it is useful to represent curves using a set of parametric equations that express the coordinates of the points of the curve as functions of the parameters. For example, in your trigonometry course you should have studied that the equation of the circle of radius $$r$$, and centered at the origin has the following form:


$$x^2+y^2=r^2.$$

The points $$(x,y)$$  that satisfy this equation are the points that form the circle of radius $$r$$ and center at the origin. For example, the circle with $$r=2$$ and center at the origin has the equation


$$x^2+y^2=4,$$

and its points are the ordered pairs $$(x,y)$$ that satisfy this equation. A parametric representation of the coordinates of the points in the circle of radius $$r$$ and center at the origin is:



$$x=r \cos(t)$$

$$y=r \sin(t),$$

where $$t$$ is a parameter that corresponds to the measure (in radians) of the positive angle with initial side that coincides with the positive part of the $$x$$-axis and the terminal side that contains the point $$(x,y)$$, as illustrated in Figure 1.


---

![figura1.jpg](images/circuloAngulo01.png)

**Figure 1.** Circle of radius $$r$$ and centered at the origin.



---

To plot a curve that is described by parametric equations, we compute the $$x$$ and $$y$$ values for a set of values of the parameter. For example, Figure 2 shows the $$t$$, $$x$$ and $$y$$ values for some of the points in the circle with $$r = 2$$.


---


![figura2.jpg](images/circuloPuntos01.png)

**Figure 2.** Some of the coordinates of the points $$(x,y)$$ of the circle of radius $$r=2$$ and centered at the origin.

---

---


!INCLUDE "../../eip-diagnostic/pretty-plots/en/diag-pretty-plots-01.html"
<br>

!INCLUDE "../../eip-diagnostic/pretty-plots/en/diag-pretty-plots-02.html"
<br>

!INCLUDE "../../eip-diagnostic/pretty-plots/en/diag-pretty-plots-03.html"
<br>

!INCLUDE "../../eip-diagnostic/pretty-plots/en/diag-pretty-plots-04.html"
<br>

!INCLUDE "../../eip-diagnostic/pretty-plots/en/diag-pretty-plots-05.html"
<br>

!INCLUDE "../../eip-diagnostic/pretty-plots/en/diag-pretty-plots-06.html"
<br>

!INCLUDE "../../eip-diagnostic/pretty-plots/en/diag-pretty-plots-07.html"
<br>


---

---



## Laboratory Session:

### Exercise 1 - Plotting Interesting Curves


#### Instructions

1. Load the project `prettyPlot` into `QtCreator`. There are two ways to do this:

	* Using the virtual machine: Double click the file `prettyPlot.pro` located in the folder `/home/eip/labs/expressions-prettyplots` of your virtual machine.
	* Downloading the project’s folder from `Bitbucket`: Use a terminal and write the command `git clone http:/bitbucket.org/eip-uprrp/expressions-prettyplots` to download the folder `expressions-prettyplots` from `Bitbucket`. Double click the file `prettyPlot.pro` located in the folder that you downloaded to your computer.

2. Configure the project and run the program by clicking the green arrow in the menu on the left side of the Qt Creator window. The program should display a window similar to the one in Figure 3.

	---

	![figura3.png](images/segment01.png)

	**Figure 3.** Line segment displayed by the program <i>PrettyPlot</i>.

	---

3. The file `main.cpp` (in Sources) contains the function `main()` where you will be adding code. Open this file and study the code.  
	
	```cpp
	QApplication a(argc, argv);
	XYPlotWindow wLine;
	XYPlotWindow wCircle;
	XYPlotWindow wHeart;
	XYPlotWindow wButterfly;

	double y = 0.00;
	double x = 0.00;
	double increment = 0.01;

	for (double t = 0; t < 2*M_PI; t = t + increment) {
    	// parametric equations
    	x = t;
    	y = t;

    	// add x and y as a point in the graph
    	wLine.AddPointToGraph(x,y);
	}

	// After all the points have been added, plot and show the graph
	wLine.Plot();
	wLine.show();
	```
    
    The line  `XYPlotWindow wLine;` creates the object `wLine`, which is the window that will show the plot of a graph, in this case the graph of a segment. Look at the `for` loop. In this cycle several values for $$t$$ are generated and a value for $$x$$ and $$y$$ is computed for each $$t$$. Each ordered pair $$(x,y)$$ is added to the graph of the segment by the method `AddPointToGraph(x,y)`.  After the cycle, there is a call to the  method  `Plot()`, to "draw" the points on the graph, and to the method `show()`, to show the plot. The *methods* are functions that allow us to work with the data of an object. Note that each of the methods is written after `wLine`, and followed by a period. In a future laboratory experience you will learn more about objects and you will practice how to create them and invoke their methods.

	The expressions for $$x$$ and $$y$$ are parametric equations for the line that passes through the origin and has the same value for $$x$$ and $$y$$. Explain why this line only goes from 0 to approximately 6.

4.	You will now write the code needed to plot a circle. The line `XYPlotWindow wCircle;` creates the object `wCircle`  for the window that will contain the plot of the circle. Using the code that plotted the segment as inspiration, write the necessary code for your program to graph a circle of radius 3 centered at the origin. Run your program and, if it is necessary, modify the code until you get the right plot. Remember that the circle should be plotted inside the `wCircle` object. Thus, when you invoke the `AddPointToGraph(x,y)`, `Plot` and `show` methods, they should be preceeded by `wCircle`; for example, `wCircle.show()`.

5. Your next task is to plot a curve with the following parametric equations:

	$$x=16 \sin^3(t)$$

	$$y=13 \cos(t) - 5 \cos(2t) - 2 \cos(3t) - \cos(4t)-3.$$

	If you implement the equations correctly, you will see the image of a heart. This plot should be obtained inside an `XYPlotWindow` object called `wHeart`.

6.  You will now plot the curve of the following parametric equations:

	$$x=5\cos(t) \left[ \sin^2(1.2t) + \cos^3(6t) \right]$$

	$$y= 10\sin(t) \left[ \sin^2(1.2t) +  \cos^3(6t) \right].$$

	Note that both expressions are almost the same, the only difference is that one starts  with $$5\cos(t)$$ and the other with $$10\sin(t)$$. Instead of computing $$ \sin^2(1.2t) + \cos^3(6t)$$ twice, you can assign its value to another variable $$q$$ and compute $$x$$ and $$y$$ as follows:

	$$q =  \sin^2(1.2t) + \cos^3(6t)$$

	$$x = 5 \cos(t)(q)$$

	$$y = 10  \sin(t)(q).$$

	Implement the above expressions, change the condition for termination of the  `for` loop to  `t < 16*M_PI`, and look at the plot that is displayed. It should look like a butterfly. This plot should be obtained inside an `XYPlotWindow` object called `wButterfly`.


In [2] and [3] you can find other parametric equations of interesting curves.



### Exercise 2 - Computing the Grade Point Average (GPA)

In this exercise you will write a program to obtain a student's grade point average (GPA). Suppose that all courses in Yauco's University are $$3$$ credits each and have the following point values: $$A = 4$$ points per credit; $$B = 3$$ points per credit; $$C = 2$$ points per credit; $$D = 1$$ point per credit and $$F = 0$$ points per credit.

#### Instructions

1. Start a new "Non-Qt" project called Average. Your `main()` function will contain the necessary code to ask the user for the number of A's, B's, C's, D's and F's obtained and compute the grade point average (GPA).

2. Your code should define the **constants**  $$A=4, B=3, C=2, D=1, F=0$$ for the points per credit, and ask the user to input the values for the variables  $$NumA$$, $$NumB$$, $$NumC$$, $$NumD$$, $$NumF$$. The variable $$NumA$$ represents the number of courses in which the student obtained $$A$$, $$NumB$$ represents the number of courses in which the student obtained $$B$$, etc. The program should display the GPA using the 0-4 point scale.

	**Hints:**

	a. You can obtain the GPA by adding the credit points corresponding to the grades (for example, an A in a 3 credit course has a value of 12 points), and dividing this sum by the total number of credits.

	b. Remember that, in C++, when both operands in the division are integers, the result will also be an integer; the remainder will be discarded. Use "type casting": `static_cast<type>(expression)` to solve this problem.
	   
3. Verify your program by computing the GPA of a student that has two A's and 2 B's; what is the average of this student? When your program is correct, save the `main.cpp` file.  

---

---

## Deliverables

1. Use "Deliverable 1" in Moodle to submit the file  `main.cpp` containing the code with the parametric equations for the graphs of the circle, the heart, and the butterfly. Remember to use good programming practices, include the names of the programmers and document your program.

2. Use "Deliverable 2"  in Moodle to submit the file `main.cpp` with the code to compute grade average. Remember to follow the instructions regarding the names and types of the variables, include the names of the programmers, document your program and use good programming practices.

---

---


## References:

[1] http://mathworld.wolfram.com/ParametricEquations.html

[2] http://paulbourke.net/geometry/butterfly/

[3] http://en.wikipedia.org/wiki/Parametric_equation